/* eslint-disable import/no-extraneous-dependencies,global-require */

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const postcssImport = require('postcss-import');
const bemLinter = require('postcss-bem-linter');
const postcssCssnext = require('postcss-cssnext');
const cssnano = require('cssnano');
const postcssReporter = require('postcss-reporter');
const postcssPresetEnv = require('postcss-preset-env');

module.exports = ({ options }) => ({
  map: options.map,
  parser: options.parser,
  plugins: [
    postcssImport({ addDependencyTo: 'webpack' }),
    postcssCssnext({
      warnForDuplicates: false,
      features: {
        customProperties: {
          variables: {
            'base-font-size': '16px',
            'max-dark-grey': '#7C888A',
            'max-base-color': '#EAF1F7',
            'max-green': '#19b391',
            'max-dark-green': '#169D7F',
            'max-super-dark-grey': '#697577',
            'max-orange': '#EFB028',
            'max-white': '#FFFFFF',
            'max-light-grey': '#B4BDC2',
            'max-border-radius': '4px;'
          }
        }
      }
    }),
    cssnano(),
    bemLinter('bem'),
    postcssReporter(),
    postcssPresetEnv(/* pluginOptions */)
  ]
});
