// @flow

/* eslint-disable no-param-reassign */

/*
 * Riderman de Sousa Barbosa
 * Copyright (C) 2018 Riderman de Sousa Barbosa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { types, flow } from 'mobx-state-tree';
import numeral from 'numeral';
import moment from 'moment';
import 'numeral/locales/pt-br';
import { getSearchFlight, getFlights } from 'api';
import airportsDB from './airports.json';

numeral.locale('pt-br');

export const Flight = types
  .model('Flight', {
    id: types.identifier,
    airlineName: types.string,
    airlineNumber: types.string,
    departureTime: types.number,
    departureAirport: types.string,
    tripStops: types.string,
    arrivalTime: types.number,
    price: types.number,
    finalPrice: types.number,
    arrivalAirport: types.string
  })
  .views(self => ({
    get priceFormatted() {
      return numeral(self.price).format('$0,0.00');
    },
    get finalPriceFormatted() {
      return numeral(self.finalPrice).format('$0,0.00');
    },
    get percentagePrice() {
      const decreaseValue = self.price - self.finalPrice;
      return decreaseValue / self.price;
    },
    get percentagePriceFormatted() {
      const diffValue = self.percentagePrice;
      return numeral(diffValue).format('0%');
    },
    get tripDuration() {
      const start = moment.unix(self.departureTime);
      const end = moment.unix(self.arrivalTime);
      const ms = end.diff(start);
      const d = moment.duration(ms);
      return Math.floor(d.asHours()) + moment(ms).format(':mm');
    }
  }));

export const Airport = types.model('Airport', {
  id: types.identifier,
  city: types.string,
  country: types.string
});

const initialDatesFiltered = [
  new Date(),
  moment()
    .add(1, 'd')
    .toDate()
];
const initialAirportsFiltered = ['CNF', 'BSB'];

const Store = types
  .model('Store', {
    datesFiltered: types.optional(
      types.array(types.Date),
      initialDatesFiltered
    ),
    airportsFiltered: types.optional(
      types.array(types.reference(Airport)),
      initialAirportsFiltered
    ),
    inboundFlights: types.optional(types.array(Flight), []),
    totalGuestsFiltered: 1,
    outboundFlights: types.optional(types.array(Flight), []),
    airports: types.optional(types.array(Airport), [])
  })
  .volatile(() => ({
    fetchError: undefined,
    isFetching: true
  }))
  .views(self => ({
    get hasInboundFlights() {
      return self.inboundFlights.length > 0;
    },
    get hasOutboundFlights() {
      return self.outboundFlights.length > 0;
    }
  }))
  .actions(self => ({
    fetchAirports() {
      const airports = Object.entries(airportsDB.airports).map(a => {
        const [id, airport] = a;
        return Airport.create({
          id,
          city: airport[0],
          country: airport[2]
        });
      });
      self.airports.replace(airports);
    },
    fetchFlights: flow(function* fetchFlights() {
      self.isFetching = true;
      self.fetchError = undefined;
      const [from, to] = self.airportsFiltered;
      const [outboundDate, inboundDate] = self.datesFiltered;

      try {
        // Get search filtered
        const resp = yield getSearchFlight(
          from.id,
          to.id,
          outboundDate,
          inboundDate,
          self.totalGuestsFiltered
        );

        self.inboundFlights.clear();
        self.outboundFlights.clear();

        const airlinesEnabled = resp.airlines.filter(f => f.status.enable);
        const promiseAll = airlinesEnabled.map(a => {
          return getFlights(resp.id, a.label).then(airlines => {
            self.addInboundAirlines(airlines.inbound);
            self.addOutboundAirlines(airlines.outbound);
            return airlines;
          });
        });

        yield Promise.all(promiseAll);
      } catch (error) {
        self.fetchError = error;
      } finally {
        self.isFetching = false;
      }
    }),
    addOutboundAirlines(outbound) {
      const flights = outbound
        // .filter(f => f.pricing.airline)
        .map(f => {
          const flight = {
            id: f.id,
            airlineName: f.airline,
            airlineNumber: f.flightNumber,
            departureTime: moment(f.departureDate).unix(),
            departureAirport: f.from,
            tripStops: f.stops === 0 ? 'VOO DIRETO' : `${f.stops} paradas`,
            arrivalTime: moment(f.arrivalDate).unix(),
            price: f.pricing.airline.saleTotal,
            finalPrice: f.pricing.miles ? f.pricing.miles.saleTotal : 0,
            arrivalAirport: f.to
          };
          return Flight.create(flight);
        });
      return flights.map(f => self.outboundFlights.push(f));
    },
    addInboundAirlines(inbound) {
      const flights = inbound
        // .filter(f => f.pricing.airline)
        .map(f => {
          const flight = {
            id: f.id,
            airlineName: f.airline,
            airlineNumber: f.flightNumber,
            departureTime: moment(f.departureDate).unix(),
            departureAirport: f.from,
            tripStops: f.stops === 0 ? 'VOO DIRETO' : `${f.stops} paradas`,
            arrivalTime: moment(f.arrivalDate).unix(),
            price: f.pricing.airline.saleTotal,
            finalPrice: f.pricing.miles ? f.pricing.miles.saleTotal : 0,
            arrivalAirport: f.to
          };
          return Flight.create(flight);
        });
      return flights.map(f => self.inboundFlights.push(f));
    },
    filterByDates(dates: Array<Date>) {
      self.datesFiltered.replace(dates);
      self.fetchFlights();
    },
    filterByGuests(guests: number) {
      self.totalGuestsFiltered = guests;
      self.fetchFlights();
    },
    filterByAirports(airports: Array<Airport>) {
      self.airportsFiltered.replace(airports);
      self.fetchFlights();
    },
    afterCreate() {
      self.fetchAirports();
      self.fetchFlights();
    }
  }));

export default Store;
