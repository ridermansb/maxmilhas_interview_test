/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import Responsive from 'react-responsive';
import Message from 'components/Message';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

type Props = {|
  children: React.Node,
  isLoading: boolean,
  error?: Error
|};

const MessageAlertSearch = () => (
  <Message title="Alerta de preço" className="col-12 col-3__desktop">
    <Mobile>
      <strong>Crie um alerta de preço para essa busca</strong>
    </Mobile>
    <Desktop>
      <strong>Crie um alerta de preço de Belo Horizonte para São Paulo.</strong>
      Quando o voo estiver no preço cadastrado, enviaremos um alerta para o seu
      e-mail e você economiza ainda mais.
      <button
        type="button"
        className="button button__primary button__icon button__block"
      >
        <i className="fas fa-arrow-right" />
        Criar Alerta
      </button>
    </Desktop>
  </Message>
);

const PageContainer = ({ children, error, isLoading }: Props) => (
  <div className="container grid grid__spaced">
    {!isLoading && <MessageAlertSearch />}
    {isLoading && (
      <div className="status">
        <i className="fas fa-spinner" />
        Loading, please wait ...
      </div>
    )}
    {!isLoading && !error && (
      <div className="col-12 col-9__desktop">{children}</div>
    )}
    {!isLoading && error ? <p>{error.message}</p> : undefined}
    <Mobile>
      <div className="button--group button--group__bottom-attached">
        <button
          type="button"
          className="col-6 button button__secondary button__icon"
        >
          <i className="fas fa-filter" />
          FILTRAR VOOS
        </button>
        <button
          type="button"
          className="col-6 button button__secondary button__icon"
        >
          <i className="fas fa-eye" />
          MILHAS DO VOO
        </button>
      </div>
    </Mobile>
  </div>
);

PageContainer.defaultProps = {
  error: undefined
};

PageContainer.displayName = 'PageContainer';

export default PageContainer;
