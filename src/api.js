// @flow

/*
 Ridermansb
 Copyright (C) 2018 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import moment from 'moment';

type HttpResponse = {
  status: number,
  statusText: string,
  json(): any
};

// eslint-disable-next-line prefer-destructuring
const API_URL = process.env.API_URL;
// eslint-disable-next-line prefer-destructuring
const API_TOKEN = process.env.API_TOKEN;

const fetchHeaders = {
  Authorization: API_TOKEN,
  'Content-Type': 'application/json; charset=utf-8',
  Accept: 'application/json'
};

function checkStatus(response: HttpResponse) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  throw new Error(response.statusText);
}

function parseJSON(response: HttpResponse) {
  return response.json();
}

export function getSearchFlight(
  from: string,
  to: string,
  outboundDate: Date,
  inboundDate: Date,
  adults: number = 1,
  cabin: string = 'EC',
  children: number = 0,
  infants: number = 0,
  date: number = Date.now()
) {
  const postData = {
    tripType: 'RT',
    from,
    to,
    outboundDate: moment(outboundDate).format('YYYY-MM-DD'),
    inboundDate: moment(inboundDate).format('YYYY-MM-DD'),
    cabin,
    children,
    adults,
    infants
  };

  return fetch(`${API_URL}/search?time=${date}`, {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'include',
    headers: new Headers(fetchHeaders),
    body: JSON.stringify(postData)
  })
    .then(checkStatus)
    .then(parseJSON);
}

export function getFlights(searchId: string, airline: string) {
  return fetch(`${API_URL}/search/${searchId}/flights?airline=${airline}`)
    .then(checkStatus)
    .then(parseJSON);
}
