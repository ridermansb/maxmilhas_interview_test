// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'core-js/stable';
import 'regenerator-runtime/runtime';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'mobx-react';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import Store from './store';

const rootEl = document.getElementById('root');
if (!rootEl) {
  throw new Error("couldn't find element with id root");
}

const store = Store.create();

const renderApp = AppComponent => {
  render(
    <Router>
      <Provider store={store}>
        <AppComponent />
      </Provider>
    </Router>,
    rootEl
  );
};
renderApp(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    // eslint-disable-next-line global-require
    const NewRoot = require('./App').default;
    renderApp(NewRoot);
  });
}
