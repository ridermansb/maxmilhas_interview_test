// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Responsive from 'react-responsive';
import './menu.css';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

const Menu = () => (
  <div styleName="Menu">
    <h1>
      <button type="button" styleName="Menu--icon">
        <i className="fas fa-bars" />
      </button>
      MaxMilhas
    </h1>

    <div styleName="Menu__center">
      <small>{__VERSION__}</small>
    </div>
    <div styleName="Menu__right">
      <Desktop>
        <div className="button--group" styleName="Menu--buttons">
          <button
            type="button"
            className="button button__transparent button__icon"
          >
            <i className="far fa-money-bill-alt" />
            Venda suas milhas
          </button>
          <button
            type="button"
            className="button button__transparent button__icon"
          >
            <i className="far fa-comment-alt" />
            Tire suas Dúvidas
          </button>
          <button
            type="button"
            className="button button__transparent button__icon"
          >
            <i className="far fa-user" />
            Login ou cadastro
          </button>
        </div>
      </Desktop>
      <Mobile>
        <button type="button" className="button button__flat">
          <i className="far fa-clock" /> 20:00
        </button>
      </Mobile>
    </div>
  </div>
);

Menu.displayName = 'Menu';

export default Menu;
