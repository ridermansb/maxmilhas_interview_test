// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import { inject } from 'mobx-react';
import autobind from 'autobind-decorator';
import { Airport } from 'store';
import './AirportFinder.css';

type Props = {
  onSelected: (airport: Airport) => void,
  title: string,
  children: React.Node,
  className?: string,
  store: {
    airports: Array<Airport>
  }
};
type State = {|
  visible: boolean
|};

@inject('store')
export default class extends React.Component<Props, State> {
  static displayName = 'AirportFinder';

  static defaultProps = {
    className: ''
  };

  state = {
    visible: false
  };

  inputEl: HTMLInputElement;

  datalistEl: HTMLDataListElement;

  @autobind
  handleClick(e: any) {
    e.preventDefault();
    this.setState(prevState => ({ ...prevState, visible: !prevState.visible }));
  }

  @autobind
  handleSubmit(e: any) {
    e.preventDefault();
    const { onSelected, store } = this.props;
    const airportName = this.inputEl.value;
    if (!airportName) {
      return;
    }
    const airport = store.airports.find(a => a.city === airportName);
    if (!airport) {
      return;
    }
    onSelected(airport);
    this.setState(prevState => ({ ...prevState, visible: !prevState.visible }));
  }

  render() {
    const { visible } = this.state;
    const { children, title, className, store } = this.props;
    const { airports } = store;
    return (
      <form
        styleName="AirportFinder"
        onSubmit={this.handleSubmit}
        className={className}
      >
        <button
          type="button"
          className="button button__transparent"
          onClick={this.handleClick}
        >
          {children}
        </button>
        {visible && (
          <div styleName="AirportFinder--finder">
            <span role="button" tabIndex="-9" onClick={this.handleClick}>
              <i className="far fa-times-circle" />
            </span>
            <label htmlFor="finder">
              {title}
              <input
                name="airports"
                type="text"
                placeholder="São Paulo - GIG"
                list="airports"
                ref={el => {
                  if (el) {
                    this.inputEl = el;
                    el.focus();
                  }
                }}
              />
              <datalist id="airports" role="button" tabIndex="-99">
                {airports.map(a => (
                  <option key={a.id} data-id={a.id} value={a.city}>
                    {a.id}
                  </option>
                ))}
              </datalist>
            </label>
            <button
              styleName="AirportFinder--button__select"
              className=""
              type="submit"
              tabIndex="-9"
            >
              Select
              <i className="fas fa-check-circle" />
            </button>
          </div>
        )}
      </form>
    );
  }
}
