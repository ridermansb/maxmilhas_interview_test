// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import Responsive from 'react-responsive';
import './message.css';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

type Props = {|
  className?: string,
  title: string,
  children?: React.Node
|};

const Message = ({ title, children, className }: Props) => (
  <div styleName="message" className={className}>
    <Mobile>
      <span>{children}</span>
      <span styleName="message--icon">
        <i className="fas fa-bell" />
      </span>
    </Mobile>
    <Desktop>
      <div styleName="message--title">
        {title}
        <i className="fas fa-bell" />
      </div>
      <div styleName="message--body">{children}</div>
    </Desktop>
  </div>
);

Message.defaultProps = {
  children: undefined,
  className: ''
};

Message.displayName = 'Message';

export default Message;
