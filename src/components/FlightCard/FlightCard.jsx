// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import moment from 'moment';
import cn from 'classnames';
import Responsive from 'react-responsive';
import { Flight } from 'store';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

type FlightCardProps = {|
  flight: Flight
|};

const PriceDetails = ({ flight }: FlightCardProps) => {
  const originalPriceClassName = cn({
    text__strikethrough: flight.percentagePrice >= 0
  });
  return (
    <React.Fragment>
      <p className={originalPriceClassName}>GOL {flight.priceFormatted}</p>
      <button type="button" className="button button__primary button__block">
        {flight.finalPriceFormatted}
      </button>
      {flight.percentagePrice >= 0 && (
        <p className="text__secondary">
          Economize {flight.percentagePriceFormatted} na MaxMilhas
        </p>
      )}
      {flight.percentagePrice < 0 && <p>Menor preço na Cia Aérea</p>}
    </React.Fragment>
  );
};

const FlightCard = ({ flight }: FlightCardProps) => (
  <div className="card">
    <div className="card--header">
      <div className="list">
        <div className="list--item">
          <h4 className="text__uppercase">{flight.airlineName}</h4>
          <div className="text__uppercase">{flight.airlineNumber}</div>
        </div>
        <div className="list--item">
          <h4>{moment.unix(flight.departureTime).format('HH:mm')}</h4>
          <div>{flight.departureAirport}</div>
        </div>
        <div className="list--item">
          <h4>{flight.tripDuration}</h4>
          <div>{flight.tripStops}</div>
        </div>
        <div className="list--item">
          <h4>{moment.unix(flight.arrivalTime).format('HH:mm')}</h4>
          <div>{flight.arrivalAirport}</div>
        </div>
        <Desktop>
          <div className="list--item">
            <PriceDetails flight={flight} />
          </div>
        </Desktop>
      </div>
    </div>
    <Mobile>
      <div className="card--body">
        <PriceDetails flight={flight} />
      </div>
      <div className="card--footer">
        <button
          type="button"
          className="button button__transparent button__icon"
        >
          <i className="fas fa-plus" />
          DETALHES DO VOO
        </button>
      </div>
    </Mobile>
  </div>
);

FlightCard.displayName = 'FlightCard';

export default FlightCard;
