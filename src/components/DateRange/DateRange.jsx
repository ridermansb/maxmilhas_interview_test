// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as React from 'react';
import Responsive from 'react-responsive';
import moment from 'moment';
import Pikaday from 'pikaday';
import autobind from 'autobind-decorator';
import 'pikaday/css/pikaday.css';
import './date-range.css';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

type MobileDateFiledProps = {|
  value: moment
|};
const MobileDateFiled = ({ value }: MobileDateFiledProps) => (
  <div styleName="DateRange--button">
    <i className="far fa-calendar-alt" />
    <span className="text__strong">{value.format('DD')}</span>
    {value.format('MMM YYYY')}
  </div>
);

type DesktopDateFieldProps = {|
  title: string,
  value: moment
|};
const DesktopDateField = ({ title, value }: DesktopDateFieldProps) => (
  <React.Fragment>
    <div styleName="DateRange--title">{title}</div>
    <div styleName="DateRange--details">
      <div>
        <h5>{value.format('DD')}</h5>
        <div styleName="DateRange--name">
          <span styleName="DateRange--month">{value.format('MMMM')}</span>
          <span>{value.format('YYYY')}</span>
        </div>
      </div>
      <i className="far fa-calendar-alt" />
    </div>
  </React.Fragment>
);

type DateProps = {|
  className?: string,
  children: React.Node,
  value: Date,
  minDate?: ?Date,
  maxDate?: ?Date,
  onChange: Date => void
|};
class DatePicker extends React.PureComponent<DateProps> {
  static defaultProps = {
    className: '',
    maxDate: undefined,
    minDate: new Date()
  };

  dateEl: HTMLDivElement;

  pikaday: Pikaday;

  componentDidMount() {
    const { onChange, value, minDate, maxDate } = this.props;
    this.pikaday = new Pikaday({
      field: this.dateEl,
      defaultDate: value,
      minDate,
      maxDate,
      format: 'DD/MM/YYYY',
      onSelect() {
        const date = this.getMoment().toDate();
        onChange(date);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    this.pikaday.setMaxDate(nextProps.maxDate);
    this.pikaday.setMinDate(nextProps.minDate);
  }

  render() {
    const { children, value, minDate, maxDate, onChange, ...rest } = this.props;

    return (
      <div
        {...rest}
        ref={el => {
          if (el) {
            this.dateEl = el;
          }
        }}
      >
        {children}
      </div>
    );
  }
}

type Props = {
  className: string,
  dates: Array<Date>,
  onChange: (Array<Date>) => void
};
type State = {|
  dates: Array<Date>
|};
// eslint-disable-next-line react/no-multi-comp
export default class extends React.PureComponent<Props, State> {
  static displayName = 'DateRange';

  state = {
    dates: []
  };

  componentWillMount() {
    const { dates } = this.props;
    this.setState(prevState => ({ ...prevState, dates }));
  }

  @autobind
  handleStartDateChange(date: Date) {
    const { dates } = this.state;
    const { onChange } = this.props;
    const [, endDate] = dates;
    const newDates = [date, endDate];
    this.setState(prevState => ({ ...prevState, dates: newDates }));
    onChange(newDates);
  }

  @autobind
  handleEndDateChange(date: Date) {
    const { dates } = this.state;
    const { onChange } = this.props;
    const [startDate] = dates;
    const newDates = [startDate, date];
    this.setState(prevState => ({ ...prevState, dates: newDates }));
    onChange(newDates);
  }

  render() {
    const { className, ...rest } = this.props;
    const { dates } = this.state;
    return (
      <div className={className} {...rest}>
        <Mobile>
          <div className="grid">
            <DatePicker
              value={dates[0]}
              minDate={new Date()}
              maxDate={dates[1]}
              onChange={this.handleStartDateChange}
              className="col-6"
            >
              <MobileDateFiled value={moment(dates[0])} />
            </DatePicker>
            <DatePicker
              value={dates[1]}
              minDate={dates[0]}
              onChange={this.handleEndDateChange}
              className="col-6"
            >
              <MobileDateFiled value={moment(dates[1])} />
            </DatePicker>
          </div>
        </Mobile>
        <Desktop>
          <div className="list">
            <DatePicker
              value={dates[0]}
              maxDate={dates[1]}
              styleName="DateRange"
              className="list--item"
              onChange={this.handleStartDateChange}
            >
              <DesktopDateField title="Ida" value={moment(dates[0])} />
            </DatePicker>
            <DatePicker
              value={dates[1]}
              minDate={dates[0]}
              styleName="DateRange"
              className="list--item"
              onChange={this.handleEndDateChange}
            >
              <DesktopDateField title="Volta" value={moment(dates[1])} />
            </DatePicker>
          </div>
        </Desktop>
      </div>
    );
  }
}
