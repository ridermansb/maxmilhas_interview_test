// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { inject } from 'mobx-react';
import AirportSelector from 'components/AirportSelector';
import DateRange from 'components/DateRange';
import Guests from 'components/Guests';
import './filter.css';

const Filter = inject('store')(({ store }) => (
  <div className="grid grid--centered" styleName="filter">
    <AirportSelector
      className="col-3 col-4__desktop"
      styleName="filter--item"
      store={store}
    />
    <DateRange
      className="col-7 col-6__desktop"
      styleName="filter--item"
      dates={store.datesFiltered}
      onChange={(dates: Array<Date>) => {
        store.filterByDates(dates);
      }}
    />
    <Guests
      className="col-2"
      styleName="filter--item"
      onGuestChanged={(guests: number) => {
        store.filterByGuests(guests);
      }}
    />
  </div>
));

Filter.displayName = 'Filter';

export default Filter;
