// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Responsive from 'react-responsive';
import FlightCard from 'components/FlightCard';
import { Flight } from 'store';
import { observer } from 'mobx-react';
import './flight-list.css';

const Desktop = props => <Responsive {...props} minWidth={768} />;

type Props = {|
  flights: Array<Flight>
|};

const FlightsList = observer(({ flights }: Props) => (
  <div styleName="FlightsList">
    <div className="list">
      <h3
        className="list--item list--item__inverted"
        styleName="FlightsList--title"
      >
        Cia Aérea
      </h3>
      <h3
        className="list--item list--item__inverted"
        styleName="FlightsList--title"
      >
        Partida
      </h3>
      <h3
        className="list--item list--item__inverted"
        styleName="FlightsList--title"
      >
        Duração
      </h3>
      <h3
        className="list--item list--item__inverted"
        styleName="FlightsList--title"
      >
        Chegada
      </h3>
      <Desktop>
        <h3
          className="list--item list--item__inverted"
          styleName="FlightsList--title"
        >
          Preço
        </h3>
      </Desktop>
    </div>
    {flights.map(f => (
      <FlightCard key={f.id} flight={f} />
    ))}
  </div>
));

FlightsList.displayName = 'FlightsList';

export default FlightsList;
