// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Responsive from 'react-responsive';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import { Airport } from 'store';
import AirportFinder from 'components/AirportFinder';
import './airport-selector.css';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

type AirportSelectorProps = {|
  title: string,
  airport: Airport
|};

const Flight = ({ title, airport }: AirportSelectorProps) => (
  <div styleName="AirportSelector">
    <p styleName="AirportSelector--title">{title}</p>
    <div styleName="AirportSelector--details">
      <div styleName="AirportSelector--name">
        <h5>{airport.city}</h5>
        <span>{airport.id}</span>
      </div>
      <i className="fas fa-map-marker-alt" />
    </div>
  </div>
);

type Props = {
  className: string,
  store: {
    airportsFiltered: Array<Airport>,
    filterByAirports: (Array<Airport>) => void
  }
};

@inject('store')
@observer
export default class extends React.Component<Props> {
  static displayName = 'AirportSelector';

  @autobind
  handleAirportFromSelected(airportFrom: Airport) {
    const { store } = this.props;
    const [, to] = store.airportsFiltered;
    const newAirports = [airportFrom, to];
    store.filterByAirports(newAirports);
  }

  @autobind
  handleAirportToSelected(airportTo: Airport) {
    const { store } = this.props;
    const [from] = store.airportsFiltered;
    const newAirports = [from, airportTo];
    store.filterByAirports(newAirports);
  }

  render() {
    const { className, store, ...rest } = this.props;
    const [airportFrom, airportTo] = store.airportsFiltered;

    return (
      <div className={className} {...rest}>
        <Mobile>
          <div className="list">
            <div className="list--item">
              <AirportFinder
                store={store}
                onSelected={this.handleAirportFromSelected}
                title="Sair de"
              >
                <span className="text__strong">
                  <i className="fas fa-map-marker-alt" />
                  {airportFrom.id}
                </span>
              </AirportFinder>
            </div>
            <div className="list--item">-</div>
            <div className="list--item">
              <AirportFinder
                store={store}
                onSelected={this.handleAirportToSelected}
                title="Ir para"
              >
                <span className="text__strong">
                  <i className="fas fa-map-marker-alt" />
                  {airportTo.id}
                </span>
              </AirportFinder>
            </div>
          </div>
        </Mobile>
        <Desktop>
          <div className="list">
            <AirportFinder
              store={store}
              onSelected={this.handleAirportFromSelected}
              title="Sair de"
              className="list--item"
            >
              <Flight title="Sair de" airport={airportFrom} />
            </AirportFinder>
            <AirportFinder
              store={store}
              onSelected={this.handleAirportToSelected}
              title="Ir para"
              className="list--item"
            >
              <Flight title="Ir para" airport={airportTo} />
            </AirportFinder>
          </div>
        </Desktop>
      </div>
    );
  }
}
