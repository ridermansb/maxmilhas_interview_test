// @flow

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PureComponent } from 'react';
import autobind from 'autobind-decorator';

type Props = {
  className: string,
  onGuestChanged: number => void
};
type State = {|
  inEditMode: boolean,
  totalGuests: number
|};

export default class extends PureComponent<Props, State> {
  static displayName = 'Guests';

  state = {
    inEditMode: false,
    totalGuests: 1
  };

  inputEl: HTMLInputElement;

  handleClick = (e: any) => {
    e.preventDefault();
    this.setState(prevState => ({ ...prevState, inEditMode: true }));
  };

  @autobind
  handleLostFocus() {
    const { onGuestChanged } = this.props;
    const totalGuests = parseInt(this.inputEl.value, 10);
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(totalGuests)) {
      return;
    }
    this.setState(prevState => ({
      ...prevState,
      inEditMode: false,
      totalGuests
    }));
    onGuestChanged(totalGuests);
  }

  render() {
    const { className, onGuestChanged, ...rest } = this.props;
    const { inEditMode, totalGuests } = this.state;

    return (
      <div
        role="button"
        tabIndex="-999"
        className={className}
        {...rest}
        onClick={this.handleClick}
      >
        <i className="fas fa-users" />
        {inEditMode && (
          <input
            type="number"
            min="1"
            max="10"
            onBlur={this.handleLostFocus}
            ref={el => {
              if (el) {
                this.inputEl = el;
              }
            }}
          />
        )}
        {!inEditMode && <span className="text__strong">{totalGuests}</span>}
      </div>
    );
  }
}
