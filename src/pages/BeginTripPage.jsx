/* eslint-disable require-jsdoc */

/*
 * Ridermansb
 * Copyright (C) 2018 Ridermansb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @flow

import React from 'react';
import { inject, observer } from 'mobx-react';
import PageContainer from 'containers/PageContainer';
import FlightsList from 'components/FlightsList';
import { Flight } from 'store';

type Props = {|
  store: {
    outboundFlights: Array<Flight>,
    isFetching: boolean,
    hasOutboundFlights: boolean,
    fetchError?: Error
  }
|};

const BeginTripPage = inject('store')(
  observer(({ store }: Props) => (
    <PageContainer isLoading={store.isFetching}>
      {store.hasOutboundFlights && (
        <FlightsList flights={store.outboundFlights} />
      )}
    </PageContainer>
  ))
);

BeginTripPage.displayName = 'BeginTripPage';

export default BeginTripPage;
