// @flow

/*
 Ridermansb
 Copyright (C) 2018 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { Fragment } from 'react';
import { NavLink, Switch, Route, withRouter } from 'react-router-dom';
import Menu from 'components/Menu';
import Filter from 'components/Filter';
import fontawesome from '@fortawesome/fontawesome';
import {
  faClock,
  faPlusSquare,
  faMoneyBillAlt,
  faTimesCircle
} from '@fortawesome/fontawesome-free-regular';
import {
  faMapMarkerAlt,
  faBars,
  faCalendarAlt,
  faUsers,
  faBell,
  faPlus,
  faFilter,
  faEye,
  faCommentAlt,
  faUser,
  faArrowRight,
  faCheckCircle,
  faSpinner
} from '@fortawesome/fontawesome-free-solid';
import Loadable from 'react-loadable';
import './assets/style.css';

// region Icons

fontawesome.library.add(faBars);
fontawesome.library.add(faPlusSquare);
fontawesome.library.add(faClock);
fontawesome.library.add(faMapMarkerAlt);
fontawesome.library.add(faCalendarAlt);
fontawesome.library.add(faUsers);
fontawesome.library.add(faBell);
fontawesome.library.add(faPlus);
fontawesome.library.add(faFilter);
fontawesome.library.add(faEye);
fontawesome.library.add(faMoneyBillAlt);
fontawesome.library.add(faCommentAlt);
fontawesome.library.add(faUser);
fontawesome.library.add(faArrowRight);
fontawesome.library.add(faTimesCircle);
fontawesome.library.add(faCheckCircle);
fontawesome.library.add(faSpinner);

// endregion

const Loading = () => <div>Loading...</div>;

const BeginTrip = Loadable({
  loader: () => import('pages/BeginTripPage'),
  loading: Loading
});

const ReturnTrip = Loadable({
  loader: () => import('pages/ReturnTripPage'),
  loading: Loading
});

type MaxNavLinkProps = {|
  to: string,
  children: string
|};
const MaxNavLink = ({ to, children }: MaxNavLinkProps) => (
  <NavLink
    exact
    to={to}
    activeClassName="tabs--item__active"
    className="tabs--item"
  >
    {children}
  </NavLink>
);

const App = withRouter(({ location }) => (
  <Fragment>
    <Menu />
    <Filter />
    <div className="tabs">
      <MaxNavLink to="/">Selecione sua ida</MaxNavLink>
      <MaxNavLink to="/returntrip">Selecione sua volta</MaxNavLink>
    </div>
    <Switch location={location}>
      <Route exact path="/" component={BeginTrip} />
      <Route exact path="/returntrip" component={ReturnTrip} />
    </Switch>
  </Fragment>
));

// export default hot(App);
// export default hot(module)(App);
export default App;
