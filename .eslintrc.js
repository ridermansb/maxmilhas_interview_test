module.exports = {
  extends: [
    'plugin:promise/recommended',
    'airbnb',
    'prettier',
    'prettier/react'
  ],
  parser: 'babel-eslint',
  settings: {
    "flowtype": {
      "onlyFilesWithFlowAnnotation": true
    },
    'import/resolver': {
      'babel-module': {
        extensions: ['.js', '.jsx', '.es', '.es6', '.mjs'],
        root: ['./src']
      }
    }
  },
  parserOptions: {
    ecmaVersion: 7,
    ecmaFeatures: { legacyDecorators: true, jsx: true },
    sourceType: 'module'
  },
  env: {
    commonjs: true,
    browser: true,
    es6: true
  },
  plugins: ['import', 'promise', 'prettier', 'react', 'jsx-a11y'],
  rules: {
    'prettier/prettier': 'error',
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['to']
      }
    ]
  },
  globals: {
    '__PRODUCTION__': true,
    '__DEVELOPMENT__': true,
    '__VERSION__': true
  }
};
