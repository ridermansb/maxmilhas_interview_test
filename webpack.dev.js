/* eslint-disable import/no-extraneous-dependencies */

/*
 Ridermansb
 Copyright (C) 2018 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define common configuration for Webpack
 * @see https://webpack.js.org/guides/production/
 * @type {webpack}
 */

const webpack = require('webpack');
const merge = require('webpack-merge');
const { join, resolve } = require('path');
const common = require('./webpack.common.js');

const host = 'localhost';
const port = 3003;
const srcFolder = resolve(__dirname, 'src');

require('dotenv').config();

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-cheap-module-source-map',
  output: {
    path: resolve('dist'),
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: './',
    sourceMapFilename: '[name].js.map',
    pathinfo: true
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: /node_modules/,
        include: [
          resolve(srcFolder, 'components'),
          resolve(srcFolder, 'containers')
        ],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            query: {
              sourceMap: true,
              importLoaders: 1,
              modules: true,
              localIdentName: 'COMP-[name]__[local]'
            }
          },
          { loader: 'postcss-loader', options: { sourceMap: true } }
        ]
      },
      {
        test: /\.css$/,
        include: [/node_modules/, resolve(srcFolder, 'assets')],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            query: { sourceMap: false }
          },
          { loader: 'postcss-loader', options: { sourceMap: true } }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEVELOPMENT__: JSON.stringify(true),
      __PRODUCTION__: JSON.stringify(false),
      __VERSION__: JSON.stringify('WIP')
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DllReferencePlugin({
      context: join(__dirname, 'dist'),
      manifest: join(__dirname, 'vendor-manifest.json')
    })
  ],
  profile: true,
  stats: {
    // Examine all modules
    maxModules: Infinity,
    // Display bailout reasons
    optimizationBailout: true
  },
  devServer: {
    contentBase: join(__dirname, 'src', 'assets'),
    publicPath: '/',
    overlay: true,
    compress: true,
    host,
    port,
    hot: true,
    historyApiFallback: true,
    watchOptions: { aggregateTimeout: 300, poll: 1000 },
    disableHostCheck: true,
    stats: 'errors-only',
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': '*'
    },
    after() {
      // eslint-disable-next-line no-console
      console.log(`Server ready on port ${port}!`);
    }
  }
});
