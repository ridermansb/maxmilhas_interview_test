/* eslint-disable import/no-extraneous-dependencies */

/*
 Ridermansb
 Copyright (C) 2018 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define common configuration for Webpack
 * @see https://webpack.js.org/guides/production/
 * @type {webpack}
 */

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { join, resolve } = require('path');

require('dotenv').config();

// eslint-disable-next-line prefer-destructuring
const API_URL = process.env.API_URL;
if (!API_URL) {
  throw new Error('API_URL not defined!');
}

// eslint-disable-next-line prefer-destructuring
const API_TOKEN = process.env.API_TOKEN;
if (!API_TOKEN) {
  throw new Error('API_TOKEN not configured!');
}

const srcFolder = resolve(__dirname, 'src');

module.exports = {
  entry: join(srcFolder, 'index.jsx'),
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      API_TOKEN: undefined,
      API_URL: '//flight-price-hmg.maxmilhas.com.br'
    }),
    new HtmlWebpackPlugin({
      title:
        'Compra e venda de passagens aéreas emitidas por milhas | MaxMilhas',
      favicon: resolve(__dirname, 'favicon.ico'),
      template: resolve(__dirname, 'src', 'index.tpl.html'),
      chunksSortMode: 'dependency'
    })
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.es', '.es6', '.mjs']
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        include: [srcFolder],
        use: {
          loader: 'eslint-loader'
        }
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        include: [srcFolder],
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true // important for performance
          }
        }
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: { interpolate: true }
        },
        exclude: [join(srcFolder, 'index.tpl.html')]
      },
      {
        test: /\.(eot|woff|woff2|ttf)$/,
        use: {
          loader: 'file-loader',
          query: {
            limit: 30000,
            name: '[name].[hash:8].[ext]',
            outputPath: 'assets/fonts/'
          }
        }
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        loaders: [
          {
            loader: 'file-loader',
            query: { outputPath: 'assets/images/' }
          }
        ]
      }
    ]
  }
};
