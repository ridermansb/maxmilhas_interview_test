/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */

const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const { join, resolve } = require('path');

module.exports = {
  mode: 'production',
  entry: {
    vendor: [
      'react',
      'react-dom',
      'react-router',
      'react-router-dom',
      'mobx',
      'mobx-react',
      'mobx-state-tree'
    ]
  },

  output: {
    filename: '[name].[hash].bundle.js',
    path: resolve(__dirname, 'dist'),
    library: '[name]'
  },
  plugins: [
    new webpack.DllPlugin({
      context: __dirname,
      name: '[name]',
      path: join(__dirname, '[name]-manifest.json')
    }),
    new CompressionPlugin({
      test: /\.(js|html)$/
    })
  ]
};
