> Resolution of MaxMilhas challenge

![mobile.png](mobile.png)
![](desktop.png)

# Get starter

> Make sure you have `.env` file with `API_TOKEN` defined first.

 - `npm i`
 - `npm run build:vendors`
 - `npm start`

# Run with Docker

 - `npm i`
 - `npm run build:vendors`
 - `npm run build`
 - `docker build . -t maxmilhas`
 - `docker run -d -p 8888:80 maxmilhas`

> App running over port 8888

# Stack

 - [Webpack 4][2]
 - React Hot Reload
 - [DLL Plugin/reference][7]
 - `import()` feature, [lazy load][3] components
 - [FlowJS][4] for typing check
 - Mobx and [mobx-state-tree][5] for state manager
 - CSS Variables
 - [CSS Modules][1]
 - PostCSS
 - Responsive with Media Query and [react-responsive][6] 

[1]: https://github.com/css-modules/css-modules
[2]: https://medium.com/webpack/webpack-4-released-today-6cdb994702d4
[3]: https://webpack.js.org/guides/code-splitting/#dynamic-imports
[4]: https://flow.org/en/docs/react/
[5]: https://github.com/mobxjs/mobx-state-tree
[6]: https://github.com/contra/react-responsive
[7]: https://webpack.js.org/plugins/dll-plugin/
