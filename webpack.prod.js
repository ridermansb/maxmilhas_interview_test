/* eslint-disable import/no-extraneous-dependencies */

/*
 Ridermansb
 Copyright (C) 2018 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define common configuration for Webpack
 * @see https://webpack.js.org/guides/production/
 * @type {webpack}
 */

const webpack = require('webpack');
const merge = require('webpack-merge');
const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { resolve } = require('path');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const common = require('./webpack.common.js');

const srcFolder = resolve(__dirname, 'src');
const gitRevisionPlugin = new GitRevisionPlugin({
  branch: true
});

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  output: {
    path: resolve('dist'),
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[chunkhash].js',
    sourceMapFilename: '[name]-[hash].js.map',
    pathinfo: true
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: /node_modules/,
        include: [
          resolve(srcFolder, 'components'),
          resolve(srcFolder, 'containers')
        ],
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              importLoaders: 1,
              modules: true,
              localIdentName: 'COMP-[name]__[local]___[hash:base64:5]'
            }
          },
          { loader: 'postcss-loader', options: { sourceMap: false } }
        ]
      },
      {
        test: /\.css$/,
        include: [/node_modules/, resolve(srcFolder, 'assets')],
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            query: { sourceMap: false }
          },
          { loader: 'postcss-loader', options: { sourceMap: true } }
        ]
      }
    ]
  },
  plugins: [
    gitRevisionPlugin,
    new webpack.DefinePlugin({
      __PRODUCTION__: JSON.stringify(true),
      __DEVELOPMENT__: JSON.stringify(false),
      __VERSION__: JSON.stringify(
        process.env.HEAD || gitRevisionPlugin.branch()
      )
    }),
    new webpack.HashedModuleIdsPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css',
      chunkFilename: '[id]-[chunkhash].css'
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new CompressionPlugin({
      test: /\.(js|css|html)$/
    })
  ],
  optimization: {
    usedExports: true,
    // runtimeChunk: true,
    // runtimeChunk: 'single',
    minimize: true,
    namedModules: true,
    splitChunks: {
      chunks: 'initial',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        commons: {
          name: 'commons', // The name of the chunk containing all common code
          minChunks: 2, // This is the number of modules
          chunks: 'async',
          priority: 10,
          reuseExistingChunk: true,
          enforce: true
        },

        vendor: {
          chunks: 'all',
          minSize: 1000,
          maxSize: 100000,
          test: /[\\/]node_modules[\\/]/i,
          name(module) {
            const packageName = module.context.match(
              /[\\/]node_modules[\\/](.*?)([\\/]|$)/
            )[1];
            return `npm.${packageName.replace('@', '')}`;
          }
        },

        styles: {
          name: 'styles',
          test: /\.s?css$/,
          chunks: 'all',
          minChunks: 1,
          reuseExistingChunk: true,
          enforce: true
        }
      }
    },
    runtimeChunk: {
      name: entrypoint => `runtime~${entrypoint.name}`
    }
  }
  // performance: {
  //   hints: 'error',
  //   maxAssetSize: 102400
  // }
});
