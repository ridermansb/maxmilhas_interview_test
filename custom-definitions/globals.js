declare var module : {
  hot : {
    accept(path:string, callback:() => void): void;
  };
};

declare module CSSModule {
  declare var exports: { [key: string]: string };
}
